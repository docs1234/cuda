#!/usr/bin/env python3

import argparse
from subprocess import run
import os

def main(versions):
    os.makedirs("versions", exist_ok=True)
    os.chdir("versions")
    for version in versions:
        os.makedirs(version, exist_ok=True)
        os.chdir(version)

        generate_single_cmd = "../../scripts/single.sh {version}".format(version=version)
        run(generate_single_cmd.split())

        os.chdir("..")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Cuda docs generator script',
        description='Generate cuda docs')
    parser.add_argument("-v", "--versions", nargs='+', default=["12.4.0"], help="versions")
    args = parser.parse_args()
    main(args.versions)
    
