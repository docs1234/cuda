#!/usr/bin/env python3

import argparse
import os
import sqlite3
import sphobjinv as soi
from subprocess import run

ROLE_TO_TYPE_DICT = { "class": "Class",
                      "enum": "Enum",
                      "enumerator": "Enum", 
                      "function": "Function",
                      "macro": "Macro",
                      "member": "Attribute",
                      "struct": "Struct",
                      "type": "Type" }

ROLES_TO_SKIP = set(["functionParam", "templateParam"])

def main():
    sqlite_file = "docSet.dsidx"
    if os.path.exists(sqlite_file):
        os.remove(sqlite_file)
    connection = sqlite3.connect(sqlite_file)
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, path TEXT);")
    cursor.execute("CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path);")
    connection.commit()

    os.chdir("docs")

    dirs_to_traverse = list()
    for item in os.listdir():
        if item[0] != '_' and os.path.isdir(item):
            dirs_to_traverse.append(item)

    for dirname in dirs_to_traverse:
        os.chdir(dirname)
        if "objects.inv" not in os.listdir():
            os.chdir("..")
            continue

        objs_txt_cmd = "sphobjinv convert plain objects.inv objs.txt"
        run(objs_txt_cmd.split())

        inv = soi.Inventory("objects.inv")
        for obj in inv.objects:
            if int(obj.priority) <= 0:
                continue
            name = obj.name
            if obj.role not in ROLE_TO_TYPE_DICT and obj.role not in ROLES_TO_SKIP:
                print("Role \"" + obj.role + "\" does not exist for following inventory object:")
                print(obj)
                exit(1)
            if obj.role in ROLES_TO_SKIP:
                continue
            typestr = ROLE_TO_TYPE_DICT[obj.role]
            path = dirname + "/" + obj.uri
            entry = (name, typestr, path)
            values = [entry]
            cursor.executemany("INSERT OR IGNORE INTO searchIndex(name, type, path) VALUES (?, ?, ?);", values)

        connection.commit()
        os.chdir("..")

    connection.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Cuda docset index build script',
        description='Generate cuda docset index')
    args = parser.parse_args()
    main()
