#!/usr/bin/env bash

if [ "$1" = "" ]; then
  echo "Please give argument for version"
  exit 1
fi

VERSION=$1
LATEST_VERSION=12.4.0

if [[ ! -d docs ]]; then
    if [[ $VERSION == $LATEST_VERSION ]]; then
        wget https://docs.nvidia.com/cuda/CUDA_${VERSION}_docs.zip
    else
        wget https://docs.nvidia.com/cuda/archive/${VERSION}/CUDA_${VERSION}_docs.zip
    fi
    mkdir -p docs
    cd docs
    unzip -q ../CUDA_${VERSION}_docs.zip
    cd ..
fi

../../scripts/build_index.py

rm -rf cuda.docset
mkdir -p cuda.docset/Contents/Resources/Documents
cp -r docs/* cuda.docset/Contents/Resources/Documents
cp docSet.dsidx cuda.docset/Contents/Resources
cp ../../templates/Info.plist cuda.docset/Contents
tar -czf cuda-docset.tgz cuda.docset
